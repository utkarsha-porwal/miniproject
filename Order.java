package com.assignment.miniproject;
import java.util.*;
import java.util.ArrayList;
public class Order {
	private int OrderId;
	private String userName="Utkarsha";
	private Date dates;
	private double totalBill;
    private ArrayList<menus> items;
	public ArrayList<menus> getItems() {
		return items;
	}
	
	public void setItems(ArrayList<menus> items) {
		this.items = items;
	}
	public int getOrderId() {
		return OrderId;
	}
	public void setOrderId(int orderId) {
		OrderId = orderId;
	}


	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getDates() {
		return dates;
	}
	public void setDates(Date dates) {
		this.dates= new Date();
	}
	public double getTotalBill() {
		return totalBill;
	}
	public void setTotalBill(double totalBill) {
		this.totalBill += totalBill;
	}
	public void createOrder(String userEmail,ArrayList<menus> items) {
		setUserName(userEmail);
		setDates(new Date());
		setItems(items);
		items.forEach((i) -> setTotalBill(i.getPrice()));
	}
	void updateOrder(menus item) {
		items.add(item);
	}
	public void displayBill() {
		System.out.println("Thanks Mr "+getUserName()+" for dinning in with Surabi");
		System.out.println("Items You have selected:- ");
		items.forEach((e) -> System.out.println(e));
		System.out.println("You total bill will be:-  "+getTotalBill());
		
		
	}
	@Override
	public String toString() {
		return "Order [OrderId=" + OrderId + ", userName=" + userName + ", dates=" + dates + ", totalBill=" + totalBill
				+ ", items=" + items + "]";
	}

  

}
