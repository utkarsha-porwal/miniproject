package com.assignment.miniproject;

import java.util.HashMap;
import java.util.Map;



public class MenuList {
	private Map <Integer,menus> menu = new HashMap<>(); 
	public MenuList()
	{
		menu.put(1,new menus(1,"Dosa",1,250));
		menu.put(2,new menus(2,"Malai Kofta",2,190));
		menu.put(3,new menus(3,"Stuffed Paratha",2,120));
		menu.put(4,new menus(4,"Jalebi",2,100));
		menu.put(5,new menus(5,"Gulab jamun",2,150));
	}
	public void displayMenu() {
		System.out.println("Todays Menu ");
		menu.forEach((k,v) -> System.out.println(v));
	}
	public menus getOrderItem(int itemId) {
		return menu.get(itemId);
	}


}
