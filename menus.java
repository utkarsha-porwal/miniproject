package com.assignment.miniproject;

public class menus {
	private int fId;
	private String fName;
	private int quantity;
	private float Price;
	
	public menus(int fId, String fName, int quantity, float price) {
		super();
		this.fId = fId;
		this.fName = fName;
		this.quantity = quantity;
		Price = price;
	}
	/**
	 * @return the fId
	 */
	public int getfId() {
		return fId;
	}
	/**
	 * @param fId the fId to set
	 */
	public void setfId(int fId) {
		this.fId = fId;
	}
	/**
	 * @return the fName
	 */
	public String getfName() {
		return fName;
	}
	/**
	 * @param fName the fName to set
	 */
	public void setfName(String fName) {
		this.fName = fName;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the price
	 */
	public float getPrice() {
		return Price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(float price) {
		Price = price;
	}
	@Override
	public String toString() {
		return "menus [fId=" + fId + ", fName=" + fName + ", quantity=" + quantity + ", Price=" + Price + "]";
	}
	

}
